Installation du Serveur Kafka sur Windows
-----------------------------------------

0- Kafka a besoin du serveur ZooKeeper pour fonctionner. ZooKeeper est un serveur indépendant qui peut être téléchargé et installer, mais Kafka embarque une
    version du serveur ZooKeeper dans son package. Nous utilisons celui-ci pour éviter d'avoir trop de configurations à faire.

1- Télécharger Kafka (https://kafka.apache.org/downloads) et dezipper le dans un repertoire de la machine (la version actuelle est la 2.50). 
    Une fois dezippé, les deux dossiers majeurs qui nous interesse sont kafka_2.12-2.5.0/bin/windows et  kafka_2.12-2.5.0/config

2- Ouvrir le fichier kafka_2.12-2.5.0/config/zookeeper.properties et changer la valeur de la propriété dataDir à dataDir=zk_data/zookeeper. Le repertoire
     de travail de ZooKeeper que nous avons appelé zk_data sera créé au démarrage de ce dernier directement dans le dossier kafka_2.12-2.5.0

3- Démarrer ZooKeeper : se placer dans le repertoire kafka_2.12-2.5.0 et ouvrir une invite de commande :
              
           > bin/windows/zookeeper-server-start.bat config/zookeeper.properties

4- Création de deux Brokers (Serveurs) Kafka broker1 et broker2 :

           - copier/coller deux fois le fichier kafka_2.12-2.5.0/config/server.properties et renommer chacun en server-broker1.properties et server-broker2.properties
           - Editer server-broker1.properties et modifier/ajouter les propriétés :
                       broker.id=1                                          (Identité unique du Broker1)
                       listerners=PLAINTEXT://localhost:9093                (le Broker 1 est accessible sur localhost:9093)
                       log.dirs=broker_logs/kafka-logs-broker1              (les dossiers broker_logs/kafka-logs-broker1 seront créés dans le dossier kafka_2.12-2.5.0)
                       delete.topic.enable=true                             (donne la possibilité de pouvoir supprimer un topic post-création)

           - Editer server-broker2.properties et modifier/ajouter les propriétés :
                       broker.id=2                                          (Identité unique du Broker2)
                       listerners=PLAINTEXT://localhost:9093                (le Broker 1 est accessible sur localhost:9094)
                       log.dirs=broker_logs/kafka-logs-broker2              (les dossiers broker_logs/kafka-logs-broker2 seront créés dans le dossier kafka_2.12-2.5.0)
                       delete.topic.enable=true                             (donne la possibilité de pouvoir supprimer un topic post-création)

5- Démarrer les 2 Brokers : se placer dans le repertoire kafka_2.12-2.5.0 et ouvrir une invite de commande :

          > bin/windows/kafka-server-start.bat config/server-broker1.properties
	      > bin/windows/kafka-server-start.bat config/server-broker2.properties

6- Création de 2 topics. 1 topic dans chaque Broker. 1 topic est divisé en 2 partitions dans 1 Broker. Dans chaque Broker on autorise la replication de ces partitions sur l'autre Broker du Cluster Kafka :

         > bin/windows/kafka-topics.bat --create --bootstrap-server localhost:9093 --replication-factor 2 -- partitions 2 --topic emarket.order.queue
         > bin/windows/kafka-topics.bat --create --bootstrap-server localhost:9094 --replication-factor 2 -- partitions 2 --topic emarket.order.bill.queue

7- On configure les permissions et les droits d'accès aux topics :

      - On donne les droits d'écriture sur les topic emarket.order.queue et emarket.order.bill.queue au producteur nommé ptest

         > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:ptest --operation write --topic emarket.order.queue
 	     
 	     > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:ptest --operation write --topic emarket.order.bill.queue

      - On donne les droits de lecture sur les topic emarket.order.queue et emarket.order.bill.queue au consommateur nommé ctest, eux appartenant au groupe cgroup
         
         > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:ctest --operation read --topic emarket.order.queue --group cgroup
 	     
 	     > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:ctest --operation read --topic emarket.order.bill.queue --group cgroup
      
8- Accésoirement pour vérifier la bonne création des topics et des acl associées :

         > bin/windows/kafka-topics.bat --describe --bootstrap-server localhost:9093 --topic emarket.order.queue
         > bin/windows/kafka-topics.bat --describe --bootstrap-server localhost:9094 --topic emarket.order.bill.queue
         
         > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --list --topic emarket.order.queue
         > bin/windows/kafka-acls.bat --authorizer-properties zookeeper.connect=localhost:2181 --list --topic emarket.order.bill.queue
