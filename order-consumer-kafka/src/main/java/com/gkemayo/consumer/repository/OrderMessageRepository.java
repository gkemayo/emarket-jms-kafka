package com.gkemayo.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.consumer.model.OrderMessage;

@Repository
public interface OrderMessageRepository extends JpaRepository<OrderMessage, Long>{

}
