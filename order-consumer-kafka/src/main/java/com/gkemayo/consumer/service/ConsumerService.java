package com.gkemayo.consumer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.gkemayo.consumer.model.Bill;
import com.gkemayo.consumer.model.OrderMessage;
import com.gkemayo.consumer.repository.BillingRepository;
import com.gkemayo.consumer.repository.OrderMessageRepository;

@Service
public class ConsumerService {
	
	@Autowired
	OrderMessageRepository consumerRepository;
	
	@Autowired
	BillingRepository billingRepository;
	
	
	@KafkaListener(topics = "${order.topic}")
	public void receiveOrder(@Payload OrderMessage orderMessage) throws InterruptedException {
		
		//simulate a long time processing orderMessage : 2 secondes
		Thread.sleep(2000);
		
		//then we archive this orderMessage in the database
		consumerRepository.save(orderMessage);
	}
	
	@KafkaListener(topics = "${order.bill.topic}")
	public void receiveOrderBill(@Payload OrderMessage orderMessage) throws InterruptedException {
		
		@SuppressWarnings("static-access")
		Bill bill = new Bill().builder().customerEmail(orderMessage.getCustomerEmail())
				                        .globalAmount(orderMessage.getItemList().stream()
				                        		                                .mapToDouble(item -> item.getPrice())
				                        		                                .sum()).build();
		//simulate a long time processing Bill : 3 seconds
		Thread.sleep(3000);
				
		//then we archive this Bill in the database
		billingRepository.save(bill);
		
		//...notify the customer that the treatment of his orderMessage has been done and send him the Bill : via an email for example.
	}

}
