package com.gkemayo.producer;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import com.gkemayo.producer.model.Item;
import com.gkemayo.producer.model.Order;

@SpringBootTest
@ActiveProfiles("test")
class OrderProducerApplicationTests {
	
	private RestTemplate restTemplate = new RestTemplate();

	@Test
	void contextLoads() {
	}
	
	@Ignore
	@SuppressWarnings("static-access")
	public void testProducerController() throws Exception {
		
		for (int i = 0; i < 100; i++) {
			
			Item item = new Item().builder()
						         .name("article"+i)
						         .quantity(i)
						         .price(100D*i).build();
			
			Order order = new Order().builder().customerName("name"+i)
								   .customerEmail("name"+i+"@yopmail.com")
								   .customerAddress(i+" Place Vendôme, 75000 France")
								   .itemList(Arrays.asList(item)).build();
			
			ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8084/producer/send/order", order, String.class);
			
			assertEquals(HttpStatus.OK, response.getStatusCode());
			assertEquals("OK", response.getBody());
		}
		
	}

}
