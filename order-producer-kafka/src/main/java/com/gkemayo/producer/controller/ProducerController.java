package com.gkemayo.producer.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.producer.model.Order;


@RestController
@RequestMapping("/producer")
@ConfigurationProperties(prefix = "order")
public class ProducerController {
	
	private static final String KO = "KO - ";

	private static final String OK = "OK";

	@Autowired
	private KafkaTemplate<String, Order> kafkaTemplate;
	
	@Value("${order.topic}")
	private String topic;
	
	@Value("${order.bill.topic}")
	private String billTopic;
	
	@PostMapping("/send/order")
	public ResponseEntity<String> submitOrder(@RequestBody Order order){
		
		try {
			order.setSendingDate(LocalDateTime.now());
			
			//send to order treatement topic
			kafkaTemplate.send(topic, order); 
			
			//send to billing topic
			kafkaTemplate.send(billTopic, order);
			
			return new ResponseEntity<String>(OK, HttpStatus.OK);
		} catch (Exception e  ) {
			return new ResponseEntity<String>(KO + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
