package com.gkemayo.producer.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String customerName;
	
	private String customerEmail;
	
	private String customerAddress;
	
	private LocalDateTime sendingDate;
	
	@Builder.Default
	List<Item> itemList = new ArrayList<>();
	

}
